(ns baboon.server
  (:require [baboon.config :as config]
            [cheshire.core :as json]
            [clj-time.core :as time]
            [clj-time.format :as timef]
            [clojure.java.io :as io]
            [compojure.core :refer [defroutes GET POST]]
            [compojure.route :as route]
            [me.raynes.fs :as fs]
            [org.httpkit.server :as server]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.defaults :refer [api-defaults wrap-defaults]]
            [ring.middleware.reload :as reload]
            [ring.middleware.stacktrace :as trace]
            [taoensso.timbre :as log]))

(def svg-route "svg_out")
(def date-fmt (:date-hour-minute-second timef/formatters))

(defn now-string []
  (let [nows (time/now)]
    (timef/unparse date-fmt nows)))

(defn save-svg [svg-string]
  (let [output-file (str svg-route "/" (now-string) ".svg")]
    (fs/touch output-file)
    (spit output-file svg-string)
    (log/info "Saved new svg at location:" output-file)))

(defn decode-response [response-stream]
  (let [body (slurp response-stream)
        body-json (json/parse-string body true)]
    (:svg body-json)))

(defn decode-and-save-svg [response-stream]
  (log/info "Saving svg...")
  (-> response-stream
      (decode-response)
      (save-svg)))

(defroutes application-routes
  (route/resources "/")
  (POST "/save-svg" the-body
        (decode-and-save-svg (:body the-body))
        {:status 200})
  (GET "/hi" []
       {:status 200
        :headers {"Content-Type" "text/html"}
        :body "hi yourself!"})
  (GET "/" []
       {:status 200
        :headers {"Content-Type" "text/html"}
        :body (slurp (io/resource "public/index.html"))}))

(defn create-application
  [app-handlers]
  (-> (wrap-defaults app-handlers api-defaults)
      (wrap-content-type)
      (wrap-cors :access-control-allow-origin [#".*"]
                 :access-control-allow-methods [:get :post :options])
      (reload/wrap-reload)
      (trace/wrap-stacktrace)
      ))

(defn -main [& [port]]
  (let [port 8080
        application (create-application application-routes)]
    (config/configure-logging)
    (server/run-server application {:port port})
    (log/info "We're up! Serving on port" port)))
