(ns baboon.config
  (:require [taoensso.timbre :as log]))

(def logging-config
  {:timestamp-opts {:pattern "yyyy-MM-dd HH:mm:ss ZZ"}})

(defn configure-logging
  []
  (let [log-level :debug]
    (log/set-level! log-level)
    (log/merge-config! logging-config)
    (log/info "Logging configs set.")))
