(ns baboon.core
  (:require [baboon.math :refer [bounded-rand-int]]
            [baboon.shapes :refer [circle star]]
            [cljs-http.client :as http]
            [reagent.core :as r]))

(enable-console-print!)

(def axis-width 500)
(def min-radius 5)
(def max-radius 35)
(def min-axis (+ 0 max-radius 1))
(def max-axis (- axis-width max-radius 1))

(def colors ["red" "blue" "green" "orange" "yellow"])

(defn svg [parts width height]
  [:svg {:style {:display "block"
                 :margin-left "auto"
                 :margin-right "auto"}
         :width width
         :height height}
   [:g parts]])

;; (defn random-circle []
;;   (let [x (bounded-rand-int min-axis max-axis)
;;         y (bounded-rand-int min-axis max-axis)
;;         r (bounded-rand-int min-radius max-radius)]
;;     [circle x y r]))

;; (defn randomized-circle [{:keys [x y r]}]
;;   [circle x y r])

(defn make-random-shape-args
  "Many shapes -- circles, polyhedra, stars -- only need a radius and a center."
  []
  {:cx (bounded-rand-int min-axis max-axis)
   :cy (bounded-rand-int min-axis max-axis)
   :r (bounded-rand-int min-radius max-radius)})

(defn make-random-polyhedron-args []
  (-> (make-random-shape-args)
      (assoc :sides (bounded-rand-int 5 12))))

(defn make-random-star-args []
  (-> (make-random-shape-args)
      (assoc :points (bounded-rand-int 5 12))))

(def number-of-circles (r/atom 25))

(defn set-number-of-circles [value]
  [:input {:type "text"
           :value @value
           :on-change #(reset! value (-> % .-target .-value js/parseInt))}])

(def sliding-num (r/atom 0))

(defn slider [value min max]
  [:input {:type "range" :value value :min min :max max
           :style {:width "100%"}
           :on-change #(reset! number-of-circles (-> % .-target .-value js/parseInt))}])

(defn submit []
  (let [svg-data (.-outerHTML (js/document.getElementById "baboon-svg"))
        params {"svg" svg-data}]
    (http/post "http://localhost:8080/save-svg" {:with-credentials? false
                                                 :json-params params})))

(defn submit-button []
  [:input {:type "button" :value "Save it!"
           :on-click submit}])

(defn app [circles]
  (let [circle-args (repeatedly @circles make-random-star-args)]
    [:div
     [:svg {:id "baboon-svg"
            :style {:display "block"
                    :margin-left "auto"
                    :margin-right "auto"}
            :width axis-width
            :height axis-width}
      (for [args circle-args] ^{:key args} [star args])]
     ;; [:p "How many circles? " [set-number-of-circles circles]]
     [slider @number-of-circles 1 100]
     [:p [submit-button]]
     ;; [:div
     ;;  [:svg {:style {:display "block"
     ;;                 :margin-left "auto"
     ;;                 :margin-right "auto"}
     ;;         :width axis-width
     ;;         :height axis-width}
     ;;   [star 250 250 100 5]]]
     ]
    ))

(r/render-component [app number-of-circles] (js/document.getElementById "svg-goes-here"))
