(ns baboon.math)

(defn derive-internal-angles [sides]
  (let [int-ang-total (* (- sides 2) 180)]
    (/ int-ang-total sides)))

(defn sin [i]
  "i is in *radians*"
  (.sin js/Math i))

(defn cos [i]
  "i is in *radians*"
  (.cos js/Math i))

(def pi (.-PI js/Math))

(defn degrees->radians [degrees]
  (* degrees (/ pi 180)))

(defn bounded-rand-int
  "Returns an int with the following behavior:
  - If the lower and upper bounds are identical, return the lower bound
  - Otherwise, return an int from the range [lower upper)"
  [lower upper]
  (cond
    (= lower upper) lower
    (< (- upper lower) 100) (rand-nth (range lower upper))
    :else (loop [n (rand-int upper)]
            (if (< n lower)
              (recur (rand-int upper))
              n))))
