(ns baboon.shapes
  (:require [baboon.math :as math :refer [cos degrees->radians derive-internal-angles sin]]
            [clojure.string :as str]))

(defn circle
  ([{:keys [cx cy r]}] (circle cx cy r))
  ([cx cy r]
   [:circle {:cx cx :cy cy :r r :stroke "black" :stroke-width 1 :fill "none"}]))

(defn rectangle [x y h w]
  [:rect {:x x :y y :width w :height h}])

(defn square [x y side]
  (rectangle x y side side))

(defn maybe-skew
  "Given a boolean and an angle, return either 0 (no skew) or between -angle and
  angle degrees of skew"
  [skew? angle]
  (if-not skew?
    0
    (math/bounded-rand-int (* -1 angle) angle)))

(defn calculate-points-for-sides
  "Given a center point as CX, CY and a radius, work out the points for an
  N-Sided shape.

  N.B., an SVG polygon needs a space-separated string of comma-separated point
  pairs e.g.: 120,5 390,102"
  [sides cx cy r & {:keys [skew?] :or {skew? true}}]
  (let [internal-angle (derive-internal-angles sides)
        central-angle (- 180 internal-angle)
        skew (degrees->radians (maybe-skew skew? central-angle))
        central-radians (degrees->radians central-angle)]
    (into []
          (for [i (range sides)
                ;; If skew is 0, this is a no-op; otherwise, consistently twist in a specific direction
                :let [curr-rads (+ skew (* central-radians i))
                      x (+ cx (* r (sin curr-rads)))
                      y (+ cy (* r (cos curr-rads)))]]
            [x y]))))

(defn stringify-polygon-points [points]
  (str/join " " (map #(str/join "," %) points)))

(defn polygon
  ([{:keys [cx cy r sides]}]
   (polygon cx cy r sides))
  ([cx cy r sides]
   (let [points (calculate-points-for-sides sides cx cy r)
         str-points (stringify-polygon-points points)]
     [:polygon {:points str-points
                :fill "none"
                :stroke "black"
                :stroke-width 1}]))
  ([cx cy r sides skew?]
   (let [points (calculate-points-for-sides sides cx cy r :skew? skew?)
         str-points (stringify-polygon-points points)]
     [:polygon {:points str-points
                :fill "none"
                :stroke "black"
                :stroke-width 1}])))

(defn hexagon
  ([cx cy r]
   [polygon cx cy r 6])
  ([{:keys [cx cy r]}]
   [polygon cx cy r 6]))

(defn nonagon
  ([cx cy r]
   [polygon cx cy r 9])
  ([{:keys [cx cy r]}]
   [polygon cx cy r 9]))

(defn star
  "The same basic operation as a polygon, but we connect the points differently.
  Note: a star with an odd number of points can be drawn as a single polygon in
  which the points connect in order. However! A start with an odd number of
  points is much more clearly represented as two separate polygons, each closed,
  each with half the points."
  ([{:keys [cx cy r points]}] (star cx cy r points))
  ([cx cy r points]
   (let [ordered-points (calculate-points-for-sides points cx cy r)
         polygon-args-map {:fill "none"
                           :stroke "black"
                           :stroke-width 1}]
     (if (odd? points)
       (let [reordered (take points (take-nth 2 (cycle ordered-points)))
             str-points (stringify-polygon-points reordered)
             args (assoc polygon-args-map :points str-points)]
         [:polygon args])
       (let [half (/ points 2)
             first-points (take half (take-nth 2 ordered-points))
             first-args (assoc polygon-args-map :points first-points)
             second-points (take half (take-nth 2 (rest ordered-points)))
             second-args (assoc polygon-args-map :points second-points)]
         [:g
          [:polygon first-args]
          [:polygon second-args]])))))
