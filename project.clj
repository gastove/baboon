(defproject baboon "0.1.0-SNAPSHOT"
  :description "what is how do i"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.562"]
                 [com.taoensso/timbre "4.10.0"]
                 [reagent "0.6.2"]
                 [compojure "1.6.0"]
                 [http-kit "2.2.0"]
                 [ring-basic-authentication "1.0.5"]
                 [ring-cors "0.1.10"]
                 [ring/ring-defaults "0.3.0"]
                 [ring/ring-devel "1.6.1"]
                 [cheshire "5.7.1"]
                 [me.raynes/fs "1.4.6"]
                 [jarohen/nomad "0.7.3"]
                 [clj-time "0.13.0"]
                 [cljs-http "0.1.43"]]
  :source-paths ["src/clj"]
  :main baboon.server
  :plugins [[lein-cljsbuild "1.1.3"]]

  :profiles {:dev {:dependencies [[figwheel-sidecar "0.5.10"]
                                  [com.cemerick/piggieback "0.2.1"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :plugins [[lein-figwheel "0.5.1"]]}}

  :cljsbuild {:builds [{:id "baboon"
                        :source-paths ["src/cljs"]
                        :figwheel true
                        :source-maps true
                        :compiler {:main "baboon.core"
                                   :asset-path "js/out"
                                   :output-to "resources/public/js/baboon.js"
                                   :output-dir "resources/public/js/out"}}]}
  ;; :figwheel {:css-dirs ["resources/public/css"]
  ;;            :open-file-command "emacsclient"
  ;;            ;; :nrepl-port 7888
  ;;            }
  )
